#!/bin/sh
# Extract the BASE_URL where the server is running, and add the URL to the APIs

BASE_URL="https://gitlab.com/api/v4/projects/" 
# Look which is the default branch
PROJECT_INFO=`curl --silent "${BASE_URL}${CI_PROJECT_ID}" --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}" --header "Content-Type: application/json"`
echo "PROJECT INFO $PROJECT_INFO"
TARGET_BRANCH=`echo $PROJECT_INFO | grep -oE -m 2 '"default_branch":"[^"]+"' | grep -oE '[^"]+'| tr " " "\n" | head -3 | tail -1`;

# The description of our new MR, we want to remove the branch after the MR has
# been closed
echo "TARGET BRANCH: $TARGET_BRANCH"
echo "PRIVATE TOKEN ${PRIVATE_TOKEN}"
# Require a list of all the merge request and take a look if there is already
# one with the same source branch
LISTMR=`curl --silent "${BASE_URL}${CI_PROJECT_ID}/merge_requests?state=opened" --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}"`;
COUNTBRANCHES=`echo ${LISTMR} | grep -o "\"source_branch\":\"${CI_COMMIT_REF_NAME}\"" | wc -l`;
EXISTING_MR_ID=`echo ${LISTMR} | grep -o "\"iid\".*\"source_branch\":\"${CI_COMMIT_REF_NAME}\"" | grep -oE "\"iid\":[0-9]+" | grep -m 1 -oE [0-9]+`;
echo $LISTMR
echo "EXISING MR ID: ${EXISTING_MR_ID}"
git fetch --all
git branch
git branch -r
CHANGED_PROJECTS=`git diff --name-only origin/${CI_COMMIT_REF_NAME} origin/${TARGET_BRANCH} | grep / | cut -d / -f 1 | uniq | sed -z 's/\n/,/g'`
REFERENCED_TICKETS=`git log --pretty=oneline origin/${CI_COMMIT_REF_NAME} ^origin/${TARGET_BRANCH} | grep -ioE proj-[0-9]+ | uniq`
echo "LABELS ${CHANGED_PROJECTS}"
echo "Count brancheds ${COUNTBRANCHES}"
echo "Referenced Tickets ${REFERENCED_TICKETS}" 
dfd
BODY="{
    \"id\": ${CI_PROJECT_ID},
    \"source_branch\": \"${CI_COMMIT_REF_NAME}\",
    \"target_branch\": \"${TARGET_BRANCH}\",
    \"remove_source_branch\": true,
    \"squash\": true,
    \"title\": \"WIP: ${CI_COMMIT_REF_NAME}\",
    \"assignee_id\":\"${GITLAB_USER_ID}\",
    \"labels\":\"${CHANGED_PROJECTS}\"

}";
# No MR found, let's create a new one
if [ ${COUNTBRANCHES} -eq "0" ]; then
    MERGE_ID=`curl -X POST "${BASE_URL}${CI_PROJECT_ID}/merge_requests" \
        --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}" \
        --header "Content-Type: application/json" \
        --data "${BODY}" | grep -oE "\"id\":[0-9]+" | grep -m 1 -oE [0-9]+`;

    echo "Attempted to open new merge request ${MERGE_ID}: ${CI_COMMIT_REF_NAME} and assigned to you";
    
else
  MERGE_UPDATE_BODY="{
    \"id\": ${CI_PROJECT_ID},
    \"merge_request_iid\": \"${EXISTING_MR_ID}\",
    \"source_branch\": \"${CI_COMMIT_REF_NAME}\",
    \"target_branch\": \"${TARGET_BRANCH}\",
    \"remove_source_branch\": true,
    \"squash\": true,
    \"title\": \"WIP: ${CI_COMMIT_REF_NAME}\",
    \"assignee_id\":\"${GITLAB_USER_ID}\",
    \"labels\":\"${CHANGED_PROJECTS}\"
  }";
echo "MERGE UPDATE BODY ${MERGE_UPDATE_BODY}"
  curl -X PUT "${BASE_URL}${CI_PROJECT_ID}/merge_requests/${EXISTING_MR_ID}" \
        --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}" \
        --header "Content-Type: application/json" \
        --data "${MERGE_UPDATE_BODY}"
  
  echo "Merge request updated.";
fi 


