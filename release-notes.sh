#!/bin/bash
#LATEST_TAG=`git tag | tail -1`
#NEXT_TAG=`git tag | tail -2 | head -1`
#REFERENCED_TICKETS=`git log --pretty=oneline ${LATEST_TAG} ^${NEXT_TAG} | grep -ioE proj-[0-9]+ | uniq`
#echo $REFERENCED_TICKETS

#!/bin/sh
# Extract the BASE_URL where the server is running, and add the URL to the APIs

BASE_URL="https://gitlab.com/api/v4/projects/"
BASE_URL_TEST="http://localhost:8000/api/v4/projects/"

TARGET_BRANCH="master"

# The description of our new MR, we want to remove the branch after the MR has
# been closed

echo "PRIVATE TOKEN ${PRIVATE_TOKEN}"
# Require a list of all the merge request and take a look if there is already
# one with the same source branch
TAGS=`curl --silent "${BASE_URL}${CI_PROJECT_ID}/repository/tags" --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}"`
PREVIOUS_TAG_DATE=`echo $TAGS | grep -oE -m 2 '"committed_date":"[^"]+"' | grep -oE '[^"]+'| tr " " "\n" | tail -1 `;
MOST_RECENT_TAG_NAME=`echo $TAGS | grep -oE -m 2 '"name":"[^"]+"' | grep -oE '[^"]+'| tr " " "\n" | head -3 | tail -1`;
echo $PREVIOUS_TAG_DATE
echo $MOST_RECENT_TAG_NAME


MERGE_IDS=`curl --silent "${BASE_URL}${CI_PROJECT_ID}/merge_requests?state=closed&target_branch=${TARGET_BRANCH}&view=simple&updated_after=${PREVIOUS_TAG_DATE}" --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}" | grep -oE "\"iid\":[0-9]+" | grep -oE '[0-9]+' `;


echo $MERGE_IDS
ALL_ITEMS=()
for MERGE_ID in $MERGE_IDS
do
    REFERENCED_TICKETS=`curl --silent "${BASE_URL}${CI_PROJECT_ID}/merge_requests/${MERGE_ID}/commits" --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}" | grep -ioE proj-[0-9]+ `;
	for TICKET in $REFERENCED_TICKETS
	do
	#GET /rest/api/2/issue/{issueIdOrKey}?fields=all
    	ALL_ITEMS=("[${TICKET}](www.google.com)<br/>" "${ALL_ITEMS[@]}")
	done
done

NEW_ITEMS=`echo "${ALL_ITEMS[@]}" | tr ' ' '\n' |  sort -u | uniq| tr '\n' ' ' `


BODY="{
    \"name\": \"Release ${MOST_RECENT_TAG_NAME}\",
    \"tag_name\": \"${MOST_RECENT_TAG_NAME}\",
    \"description\": \"# What's In This Release:\\n <br/>$NEW_ITEMS\"

}";

echo $BODY
#curl -X POST "${BASE_URL}${CI_PROJECT_ID}/releases" --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}" --header "Content-Type: application/json" --data '${BODY}'

curl --silent -X POST "${BASE_URL}${CI_PROJECT_ID}/releases" \
        --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}" \
        --header "Content-Type: application/json" \
        --data "${BODY}"


