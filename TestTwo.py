import gitlab
import os
from git import Repo
import re

import logging
logging.basicConfig(level=logging.INFO)


project_id = os.environ['CI_PROJECT_ID']
private_token = os.environ['CI_PRIVATE_TOKEN']
source_branch = os.environ['CI_COMMIT_REF_NAME']
repo_working_dir = os.environ['REPO_WORKING_DIR']
gitlab_user_id = os.environ['GITLAB_USER_ID']
target_branch = "master"
ticket_prefix = "proj-"
remove_source_branch=True

repo = Repo(repo_working_dir)
git = repo.git
gl = gitlab.Gitlab('https://gitlab.com/', private_token=private_token)
project = gl.projects.get(project_id)


tags = project.tags.list()

if len(tags) < 2:
    previous_tag_date = "1970-01-01T00:00:00.000+00:00"
else:
    previous_tag_date = tags[1].commit["committed_date"]

latest_tag_name = tags[0].name


merge_requests = project.mergerequests.list(state="closed", target_branch=target_branch, view="simple", updated_after=previous_tag_date)


referenced_tickets = []
for merge_request in merge_requests:
    for commit in merge_request.commits():
        referenced_tickets.extend(re.findall("%s[0-9]+" % ticket_prefix, commit.message))


# dedupe
referenced_tickets = list(dict.fromkeys(referenced_tickets))
print(referenced_tickets)
formatted_tickets = " ".join(["[%s](www.google.com)<br/>" % ticket for ticket in referenced_tickets])


project.releases.create({'name':"Release %s" % latest_tag_name, 'tag_name':latest_tag_name, 'description':"# What's In This Release:\n <br/>%s" % formatted_tickets})
# import logging
#
# try:
#     import http.client as http_client
# except ImportError:
#     # Python 2
#     import httplib as http_client
# http_client.HTTPConnection.debuglevel = 1
#
# # You must initialize logging, otherwise you'll not see debug output.
# logging.basicConfig()
# logging.getLogger().setLevel(logging.DEBUG)
# requests_log = logging.getLogger("requests.packages.urllib3")
# requests_log.setLevel(logging.DEBUG)
# requests_log.propagate = True


# open_merge_requests = project.mergerequests.list(state="opened")
# existing_merge_request_iid = None
# for merge_request in open_merge_requests:
#     if merge_request.source_branch == source_branch:
#         existing_merge_request_iid = merge_request.iid
#
# def get_labels():
#     pom_count = 0
#     #detect if this is a project with multiple pom.xmls
#     for root, dirs, files in os.walk(repo_working_dir):
#         for file in files:
#             if file.endswith("pom.xml"):
#                 pom_count += 1
#
#     if pom_count > 1:
#         # git.fetch("--all")
#         git_diff = git.diff("--name-only", "origin/%s" % source_branch, "origin/%s" % target_branch)
#
#         #find all the affected subfolders
#         changed_subfolders = [path.split("/")[0] for path in git_diff.split() if "/" in path]
#
#         #dedupe
#         return list(dict.fromkeys(changed_subfolders))
#     return []
#
#
# def get_referenced_tickets():
#     git_log = git.log("--pretty=oneline", "origin/%s" % source_branch, "^origin/%s" % target_branch)
#
#     tickets = re.findall("%s[0-9]+"%ticket_prefix, git_log )
#     # dedupe
#     return list(dict.fromkeys(tickets))
#
#
# labels = get_labels()
# print(labels)
# tickets = get_referenced_tickets()
# print(tickets)
#
#
# title = source_branch
# if source_branch.startswith(ticket_prefix):
#     title = "Resolved: %s" % source_branch
# if existing_merge_request_iid is None:
#     mr = project.mergerequests.create({'source_branch': source_branch,
#                                        'target_branch': target_branch,
#                                        'title': 'WIP: %s' % title,
#                                        'labels': labels,
#                                        'remove_source_branch': remove_source_branch,
#                                        'squash': True,
#                                        'assignee_id': gitlab_user_id})
#     print("Attempted to open new merge request %s: %s and assigned to you." % (mr.iid, title))
#     mr.save()
#
# else:
#     mr = project.mergerequests.get(existing_merge_request_iid)
#     mr.labels = labels
#     mr.save()
#
#     print("Updated merge request %s: %s and assigned to you." % (mr.iid, title))