module.exports = function (grunt) {

// -- Config -------------------------------------------------------------------

grunt.initConfig({

    nick : 'pure',
    pkg  : grunt.file.readJSON('package.json'),
    bower: grunt.file.readJSON('bower.json'),

    // -- bower.json Config ---------------------------------------------------------

    bower_json: {
        release: {
            values: {
                main: 'pure.css'
            },

            dest: 'css/'
        }
    },

    // -- Clean Config ---------------------------------------------------------

    clean: {
        css    : ['css/'],
        css_res: ['css/*-r.css'],
        release  : ['release/<%= pkg.version %>/']
    },

    // -- Copy Config ----------------------------------------------------------

    copy: {
        css: {
            src    : 'src/**/css/*.css',
            dest   : 'css/',
            expand : true,
            flatten: true
        },

        release: {
            src : '{LICENSE.md,README.md,HISTORY.md}',
            dest: 'css/'
        }
    },

    // -- Concat Config --------------------------------------------------------

    concat: {
        css: {
            files: [
                {'css/base.css': [
                    'bower_components/normalize-css/normalize.css',
                    'css/base.css'
                ]},

                {'css/buttons.css': [
                    'css/buttons-core.css',
                    'css/buttons.css'
                ]},

                {'css/forms-nr.css': [
                    'css/forms.css'
                ]},

                {'css/forms.css': [
                    'css/forms-nr.css',
                    'css/forms-r.css'
                ]},

                {'css/grids.css': [
                    'css/grids-core.css',
                    'css/grids-units.css'
                ]},

                {'css/menus.css': [
                    'css/menus-core.css',
                    'css/menus-horizontal.css',
                    'css/menus-dropdown.css',
                    'css/menus-scrollable.css',
                    'css/menus-skin.css',
                ]},

                // Rollups

                {'css/<%= nick %>.css': [
                    'css/base.css',
                    'css/grids.css',
                    'css/buttons.css',
                    'css/forms.css',
                    'css/menus.css',
                    'css/tables.css'
                ]},

                {'css/<%= nick %>-nr.css': [
                    'css/base.css',
                    'css/grids.css',
                    'css/buttons.css',
                    'css/forms-nr.css',
                    'css/menus.css',
                    'css/tables.css'
                ]}
            ]
        }
    },

    // -- PostCSS Config --------------------------------------------------------

    postcss: {
        options: {
            processors: [
                require('autoprefixer')({browsers: ['last 2 versions', 'ie >= 8', 'iOS >= 6', 'Android >= 4']})
            ]
        },
        dist: {
            src: 'css/*.css'
        }
    },

    // -- CSSLint Config -------------------------------------------------------

    csslint: {
        options: {
            csslintrc: '.csslintrc'
        },

        base   : ['src/base/css/*.css'],
        buttons: ['src/buttons/css/*.css'],
        forms  : ['src/forms/css/*.css'],
        grids  : ['src/grids/css/*.css'],
        menus  : ['src/menus/css/*.css'],
        tables : ['src/tables/css/*.css']
    },

    // -- CSSMin Config --------------------------------------------------------

    cssmin: {
        options: {
            noAdvanced: true
        },

        files: {
            expand: true,
            src   : 'css/*.css',
            ext   : '-min.css'
        }
    },

    // -- Compress Config ------------------------------------------------------

    compress: {
        release: {
            options: {
                archive: 'release/<%= pkg.version %>/<%= nick %>-<%= pkg.version %>.tar.gz'
            },

            expand : true,
            flatten: true,
            src    : 'css/*',
            dest   : '<%= nick %>/<%= pkg.version %>/'
        }
    },

    // -- License Config -------------------------------------------------------

    license: {
        normalize: {
            options: {
                banner: [
                    '/*!',
                    'normalize.css v<%= bower.devDependencies["normalize-css"] %> | MIT License | git.io/normalize',
                    'Copyright (c) Nicolas Gallagher and Jonathan Neal',
                    '*/\n'
                ].join('\n')
            },

            expand: true,
            cwd   : 'css/',
            src   : ['base*.css', '<%= nick %>*.css']
        },

        yahoo: {
            options: {
                banner: [
                    '/*!',
                    'Pure v<%= pkg.version %>',
                    'Copyright 2013 Yahoo!',
                    'Licensed under the BSD License.',
                    'https://github.com/pure-css/pure/blob/master/LICENSE.md',
                    '*/\n'
                ].join('\n')
            },

            expand: true,
            src   : ['css/*.css']
        }
    },

    // -- Pure Grids Units Config ----------------------------------------------

    pure_grids: {
        default_units: {
            dest: 'css/grids-units.css',

            options: {
                units: [5, 24]
            }
        },

        responsive: {
            dest: 'css/grids-responsive.css',

            options: {
                mediaQueries: {
                    sm: 'screen and (min-width: 35.5em)',   // 568px
                    md: 'screen and (min-width: 48em)',     // 768px
                    lg: 'screen and (min-width: 64em)',     // 1024px
                    xl: 'screen and (min-width: 80em)'      // 1280px
                }
            }
        }
    },

    // -- Strip Media Queries Config -------------------------------------------

    stripmq: {
        all: {
            files: {
                //follows the pattern 'destination': ['source']
                'css/grids-responsive-old-ie.css':
                    ['css/grids-responsive.css']
            }
        }
    },

    // -- CSS Selectors Config -------------------------------------------------

    css_selectors: {
        base: {
            src : 'css/base.css',
            dest: 'css/base-context.css',

            options: {
                mutations: [{prefix: '.pure'}]
            }
        }
    },

    // -- Watch/Observe Config -------------------------------------------------

    observe: {
        src: {
            files: 'src/**/css/*.css',
            tasks: ['test', 'suppress', 'css'],

            options: {
                interrupt: true
            }
        }
    }
});

// -- Main Tasks ---------------------------------------------------------------

// npm tasks.
grunt.loadNpmTasks('grunt-contrib-clean');
grunt.loadNpmTasks('grunt-contrib-copy');
grunt.loadNpmTasks('grunt-contrib-concat');
grunt.loadNpmTasks('grunt-contrib-csslint');
grunt.loadNpmTasks('grunt-contrib-cssmin');
grunt.loadNpmTasks('grunt-contrib-compress');
grunt.loadNpmTasks('grunt-contrib-watch');
grunt.loadNpmTasks('grunt-css-selectors');
grunt.loadNpmTasks('grunt-postcss');
grunt.loadNpmTasks('grunt-pure-grids');
grunt.loadNpmTasks('grunt-stripmq');

// Local tasks.
grunt.loadTasks('tasks/');

grunt.registerTask('default', ['import', 'test', 'css']);
grunt.registerTask('import', ['bower_install']);
grunt.registerTask('test', ['csslint']);
grunt.registerTask('css', [
    'clean:css',
    'copy:css',
    'pure_grids',
    'stripmq',
    'concat:css',
    'clean:css_res',
    'css_selectors:base',
    'postcss',
    'cssmin',
    'license'
]);

// Makes the `watch` task run a css first.
grunt.renameTask('watch', 'observe');
grunt.registerTask('watch', ['default', 'observe']);

grunt.registerTask('release', [
    'default',
    'clean:release',
    'copy:release',
    'bower_json:release',
    'compress:release'
]);

};
